# DevOps Apprenticeship: Project Exercise

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Create Trello Account and App key

We're going to be using Trello's API to fetch and save to-do tasks. In order to call their API, you need to first [create an account](https://trello.com/signup) on Trello, then generate an API key and token by following [the instructions here](https://trello.com/app-key).

After that it create a new board and a few example cards on the Trello web app.

Note down the board id by exploring the json file of the board which can be found by appending .json to the board url e.g. https://trello.com/b/xxx/todo-list.json.

Create following 3 environment variables in the .env file and assign appropriate values.

TRELLO_API_KEY=xxx
TRELLO_TOKEN=xx
TODO_BOARD_ID=xx

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:

```bash
$ poetry run flask run
```

You should see output similar to the following:

```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```

Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

## Tests

You can run both unit and integration tests for todo app using pytest.

Run following command from the root directory:

`$ poetry run pytest`

Or you can run them from VSCode:

Click the conical flask icon on the activity bar on the left edge of VSCode. Click the refresh icon at the top of the panel to rediscover tests. Click the play icon at the top to run all tests. Click the play icon next to a file or test name to run that file or test individually.

- Intellisense annotations for running/debugging each test should also appear above the test functions in the code.
- If test discovery fails, check that Poetry has installed your dependencies and that the Python interpreter is selected correctly - you should be using the executable from the .venv folder.

## Running the app with Docker

If you haven't already, you'll need to install Docker Desktop on your machine.

Please check out the todo app code and prepare the necessary .env file in your root directory.

#### Development docker image

To build the development docker image, run following command from the root directory:

`$docker build --target development --tag todo-app:dev .`

To execute the docker image, run following command from the root directory (source path needs to be modified as per the windows/unix host machine):

`docker run --env-file .env -p 8080:5000 --mount "type=bind,source=$(pwd)/todo_app,destination=/usr/src/app/todo_app" todo-app:dev`

After this, you can visit http://127.0.0.1:8080/ (note the port number is not 5000) in your local browser to see todo_app running through docker.

You can open the project in VS and make any changes to index.html. These should reflect in the running instance straight away without need to rebuild the image.

#### Production docker image

To build the production docker image, run following command from the root directory:

`docker build --target production --tag todo-app:prod .`

To execute the docker image run following command from the root directory (source path needs to be modified as per the windows/unix host machine):

`docker run --env-file .env --publish 8080:8080 todo-app:prod`

After this, you can visit http://127.0.0.1:8080/ in your local browser.

#### Documentation

The documentation folder contains html describing various parts of the to-do app.

These can be viewed in any standard browser like Chrome.

For editing, please visit https://app.diagrams.net/, select "Open Existing Diagram" option and to open the diagram to be edited.

#### Test docker image

To build the test docker image, run following command from the root directory

`docker build --target test --tag my-test-image .`

Once the test docker image is ready, run following commands to execute the tests inside the docker.

`docker run --env-file .env.test my-test-image tests`

`docker run --env-file .env.test my-test-image tests_e2e`

#### Deployment on Heroku

The Todo app is deployed on Heroku through CI/CD pipeline. The app can be accessed through following url -

https://aptodoapp.herokuapp.com/
