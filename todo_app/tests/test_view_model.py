"""Unit tests for viewmodel"""

import pytest

from todo_app.app import ViewModel
from todo_app.data.trello_items import Item


@pytest.fixture
def viewmodel() -> ViewModel:
    todolistitems = []
    todolistitems.append(Item(1, "test todo item1", "Doing"))
    todolistitems.append(Item(2, "test todo item2", "Doing") )
    todolistitems.append(Item(3, "test todo item3", "Done") )
    todolistitems.append(Item(4, "test todo item4", "To Do") )
    todolistitems.append(Item(5, "test todo item5", "To Do") )
    todolistitems.append(Item(6, "test todo item6", "To Do") )
    todolistitems.append(Item(7, "test todo item7", "NotReady") )
    return ViewModel(items=todolistitems)


def test_doing_items_count(viewmodel: ViewModel):
    assert len(viewmodel.doing_items)==2

def test_doing_items_LastItemmatch(viewmodel: ViewModel):
    doingitems = viewmodel.doing_items
    for item in doingitems:
        assert item.status == "Doing"

def test_done_items_count(viewmodel: ViewModel):
    assert len(viewmodel.done_items)==1

def test_done_items_LastItemmatch(viewmodel: ViewModel):
    doneitems = viewmodel.done_items
    for item in doneitems:
        assert item.status == "Done"

def test_todo_items_count(viewmodel: ViewModel):
    assert len(viewmodel.todo_items)==3

def test_todo_items_LastItemmatch(viewmodel: ViewModel):
    todoitems = viewmodel.todo_items
    for item in todoitems:
        assert item.status == "To Do"
    
def test_unknown_items_count(viewmodel: ViewModel):
    assert len(viewmodel.unknownstatusitems_items)==1

def test_unknown_items_LastItemmatch(viewmodel: ViewModel):
    unknownitems = viewmodel.unknownstatusitems_items
    for item in unknownitems:
        assert item.status == "NotReady"
