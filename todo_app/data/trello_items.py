import requests
import os


class Item:
    def __init__(self, id, name, status="To Do"):
        self.id = id
        self.name = name
        self.status = status
        self.done = status == "Done"

    @classmethod
    def from_trello_card(cls, card, list):
        return cls(card["id"], card["name"], list["name"])


def get_items():
    """
    Fetches all cards from Trello.

    Returns:
        list: The list of items and their state.
    """

    payload = {
        "key": os.getenv("TRELLO_API_KEY"),
        "token": os.getenv("TRELLO_TOKEN"),
        "cards": "open",
    }

    todoboardid = os.getenv("TODO_BOARD_ID")

    response = requests.get(
        f"https://api.trello.com/1/boards/{todoboardid}/lists/",
        payload,
    )

    json_res = response.json()

    listitems = []

    for list in json_res:
        cards = list["cards"]
        for card in cards:
            listitems.append(Item.from_trello_card(card, list))

    return listitems


def add_item(title):

    """
    Adds a new item with the specified title to the Trello board.

    Args:
        title: The title of the item.

    """

    trelloapikey = os.getenv("TRELLO_API_KEY")
    trellotoken = os.getenv("TRELLO_TOKEN")
    todolistid = os.getenv("TODO_LIST_ID")

    data = {"name": title, "idList": todolistid}

    response = requests.post(
        f"https://api.trello.com/1/cards/?key={trelloapikey}&token={trellotoken}", data
    )


def complete_item(itemid):

    """
    Adds a new item with the specified title to the Trello board.

    Args:
        title: The title of the item.

    """

    trelloapikey = os.getenv("TRELLO_API_KEY")
    trellotoken = os.getenv("TRELLO_TOKEN")
    donelistid = os.getenv("DONE_LIST_ID")

    data = {"idList": donelistid}

    response = requests.put(
        f"https://api.trello.com/1/cards/{itemid}/?key={trelloapikey}&token={trellotoken}",
        data,
    )
