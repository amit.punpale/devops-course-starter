from flask import Flask, render_template, request, redirect

from todo_app.data.trello_items import get_items, add_item, complete_item

from todo_app.flask_config import Config
import logging

def create_app():
    app = Flask(__name__)
    # We specify the full path and remove the import for this config
    # so it loads the env variables when the app is created, rather
    #than when this file is imported
    app.config.from_object(Config())

    logging.basicConfig(level=logging.DEBUG)

    @app.route("/")
    def index():
        items = get_items()
        item_view_model = ViewModel(items)
        return render_template("index.html", view_model=item_view_model)
    
    @app.route("/", methods=["POST"])
    def addToDo():
        add_item(title=request.form.get("TodoItem"))
        return redirect("/")

    @app.route("/done", methods=["POST"])
    def completeitem():
        complete_item(itemid=request.form["submit_button"])
        return redirect("/")

    return app

class ViewModel:
    def __init__(self, items):
        self._items = items
        self._doingitems = []
        self._doneitems = []
        self._todoitems = []
        self._unknownstatusitems = []

        for item in items :
            if item.status == "Doing":
                self._doingitems.append(item)
            elif item.status == "Done":
                self._doneitems.append(item)
            elif item.status == "To Do":
                self._todoitems.append(item)
            else:
                self._unknownstatusitems.append(item)

    @property
    def allitems(self):
        return self._items

    @property
    def doing_items(self):
        return self._doingitems

    @property
    def done_items(self):
        return self._doneitems

    @property
    def todo_items(self):
        return self._todoitems

    @property
    def unknownstatusitems_items(self):
        return self._unknownstatusitems