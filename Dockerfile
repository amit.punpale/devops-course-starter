
FROM python:3.10.4-buster as base
# Perform common operations, dependency installation etc...

ARG YOUR_ENV

ENV YOUR_ENV=${YOUR_ENV}

RUN pip install "poetry==1.1.12"
# Copy only requirements to cache them in docker layer
WORKDIR /code
COPY poetry.lock pyproject.toml /code/

# Project initialization:
RUN poetry config virtualenvs.create false \
    && poetry install $(test "$YOUR_ENV" == production && echo "--no-dev") --no-interaction --no-ansi

EXPOSE 8080
WORKDIR /usr/src/app


FROM base as production
# Configure for production
RUN pip install  "gunicorn"
COPY . .
CMD ["/usr/local/bin/gunicorn", "-c", "gconfig.py", "--pythonpath", "/usr/src/app/todo_app", "app:create_app()"]

FROM base as development
# Configure for local development
RUN pip install  "flask"
ENTRYPOINT FLASK_APP=/usr/src/app/todo_app/app.py flask run --host=0.0.0.0

FROM base as test
COPY . .
WORKDIR /usr/src/app/todo_app
ENTRYPOINT ["poetry", "run", "pytest"]





